package com.prueba.taxCalculator;

/**
 * Enum that defines the product types and its tax associated
 * 
 * @author jmpueyo
 *
 */
public enum ProductType {

	FOOD(0), BOOKS(0), MEDICAL(0), OTHER(10);

	// Tax value (%)
	private double taxValue;

	private ProductType(double taxValue) {
		this.taxValue = taxValue;
	}

	public double getTaxValue() {
		return this.taxValue;
	}

}

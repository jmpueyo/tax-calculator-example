package com.prueba.taxCalculator.dto;

import java.math.BigDecimal;

import com.prueba.taxCalculator.ProductType;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * POJO that contains input data about the products
 * 
 * @author jmpueyo
 *
 */
@Data
@AllArgsConstructor
public class ProductSaleDTO {
	private int numItems;
	private String name;
	private ProductType type;
	private boolean isImported;
	// Total prices of the items, not individual
	private double price;
	private BigDecimal finalPrice;

	public String toStringInputData() {
		StringBuffer sb = new StringBuffer();
		sb.append(numItems).append(isImported ? " imported " : " ").append(name).append(": ").append(price);
		return sb.toString();
	}

	public String toStringOutputData() {
		StringBuffer sb = new StringBuffer();
		sb.append(numItems).append(isImported ? " imported " : " ").append(name).append(": ").append(finalPrice);
		return sb.toString();
	}

}

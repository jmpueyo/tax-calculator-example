package com.prueba.taxCalculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import com.prueba.taxCalculator.dto.ProductSaleDTO;
import com.prueba.taxCalculator.dto.ResultProductSaleDTO;

class TaxCalculatorServiceTest {

	private TaxCalculatorService service;
	private List<ProductSaleDTO> productList;

	@BeforeEach
	void setUp(TestInfo testInfo) throws Exception {
		service = new TaxCalculatorService();
		System.out.println();
		System.out.println(testInfo.getDisplayName());
		System.out.println("========================");
	}

	@Test
	void calculateTaxPrices_1() {
		getData1();
		System.out.println("Input-------------------------------------");
		productList.forEach(p -> System.out.println(p.toStringInputData()));
		System.out.println("Output------------------------------------");
		ResultProductSaleDTO calculateTaxPrices = service.calculateTaxPrices(productList);

		assertEquals(7.65, calculateTaxPrices.getTotalTaxValueAdded().doubleValue());
		assertEquals(65.15, calculateTaxPrices.getTotalSalePrice().doubleValue());
	}

	@Test
	void calculateTaxPrices_2() {
		getData2();
		System.out.println("Input-------------------------------------");
		productList.forEach(p -> System.out.println(p.toStringInputData()));
		System.out.println("Output------------------------------------");
		ResultProductSaleDTO calculateTaxPrices = service.calculateTaxPrices(productList);

		assertEquals(7.65, calculateTaxPrices.getTotalTaxValueAdded().doubleValue());
		assertEquals(70.15, calculateTaxPrices.getTotalSalePrice().doubleValue());
	}

	@Test
	void calculateTaxPrices_3() {
		getData3();
		System.out.println("Input-------------------------------------");
		productList.forEach(p -> System.out.println(p.toStringInputData()));
		System.out.println("Output------------------------------------");
		ResultProductSaleDTO calculateTaxPrices = service.calculateTaxPrices(productList);

		assertEquals(9.20, calculateTaxPrices.getTotalTaxValueAdded().doubleValue());
		assertEquals(102.30, calculateTaxPrices.getTotalSalePrice().doubleValue());

	}

	private void getData1() {
		productList = Arrays.asList(new ProductSaleDTO(1, "box of chocolates", ProductType.FOOD, true, 10, null),
				new ProductSaleDTO(1, "bottle of perfume", ProductType.OTHER, true, 47.5, null));
	}

	private void getData2() {
		productList = Arrays.asList(new ProductSaleDTO(1, "box of chocolates", ProductType.FOOD, true, 10, null),
				new ProductSaleDTO(1, "bottle of perfume", ProductType.OTHER, true, 47.5, null),
				new ProductSaleDTO(1, "box of bandaids", ProductType.MEDICAL, false, 5, null));
	}

	private void getData3() {
		productList = Arrays.asList(new ProductSaleDTO(1, "box of chocolates", ProductType.FOOD, true, 10, null),
				new ProductSaleDTO(1, "bottle of perfume", ProductType.OTHER, true, 47.5, null),
				new ProductSaleDTO(1, "box of bandaids", ProductType.MEDICAL, false, 5, null),
				new ProductSaleDTO(3, "Lotr trilogy books", ProductType.BOOKS, true, 30.6, null));
	}

}

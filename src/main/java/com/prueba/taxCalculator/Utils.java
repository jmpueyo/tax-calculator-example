package com.prueba.taxCalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.prueba.taxCalculator.dto.ProductSaleDTO;

/**
 * Utility class
 * 
 * @author jmpueyo
 *
 */
public class Utils {

	// Tax for imported products (%)
	private static final double TAX_IMPORTED_VALUE = 5;
	// Rounding interval for the calculated price with taxes
	private static final double ROUNDING_INTERVAL = 0.05;

	/**
	 * Calculate the final price of a product:
	 * <ul>
	 * <li>Depending on its type, add the corresponding tax percentage
	 * {@link ProductType}
	 * <li>If the product is imported, add a fixed tax percentage
	 * {@link Utils#TAX_IMPORTED_VALUE}
	 * <li>
	 * </ul>
	 * 
	 * Round the final result within the nearest interval, defined in
	 * {@link Utils#ROUNDING_INTERVAL}
	 * 
	 * @param product
	 */
	public static void calculateTax(ProductSaleDTO product) {

		// Calculate total taxes applied
		double taxApplied2Product = (product.isImported() ? TAX_IMPORTED_VALUE : 0) + product.getType().getTaxValue();

		// Round product price with taxes
		double priceWithTaxes = product.getPrice() + (product.getPrice() * taxApplied2Product / 100);
		double aux = Math.round(priceWithTaxes / ROUNDING_INTERVAL) * ROUNDING_INTERVAL;
		BigDecimal priceWithTaxesRounded = BigDecimal.valueOf(aux)
				.setScale(BigDecimal.valueOf(ROUNDING_INTERVAL).scale(), RoundingMode.HALF_EVEN);
		product.setFinalPrice(priceWithTaxesRounded);
	}

}

package com.prueba.taxCalculator.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

/**
 * POJO that contains output product's data
 * 
 * @author jmpueyo
 *
 */
@Data
public class ResultProductSaleDTO {

	private List<ProductSaleDTO> productList;
	private BigDecimal totalTaxValueAdded;
	private BigDecimal totalSalePrice;
}

package com.prueba.taxCalculator;

import java.math.BigDecimal;
import java.util.List;

import com.prueba.taxCalculator.dto.ProductSaleDTO;
import com.prueba.taxCalculator.dto.ResultProductSaleDTO;

/**
 * Service that calculates the total prices of a list of products, adding the
 * corresponding taxes
 * 
 * @author jmpueyo
 *
 */
public class TaxCalculatorService {

	public ResultProductSaleDTO calculateTaxPrices(List<ProductSaleDTO> productList) {
		ResultProductSaleDTO result = new ResultProductSaleDTO();

		// 1.- Calculate prices with taxes for each entry
		productList.forEach(p -> {
			Utils.calculateTax(p);
			System.out.println(p.toStringOutputData());
		});

		// 2.- Calculate total added price due to taxes
		BigDecimal totalTaxValueAdded = productList.stream()
				.map(p -> p.getFinalPrice().subtract(BigDecimal.valueOf(p.getPrice()))).reduce(BigDecimal::add).get();
		System.out.println("Sales Taxes: " + totalTaxValueAdded);

		// 3.- Calculate final price
		BigDecimal totalSalePrice = productList.stream().map(ProductSaleDTO::getFinalPrice).reduce(BigDecimal::add)
				.get();
		System.out.println("Total: " + totalSalePrice);

		// 4.- Map results
		result.setProductList(productList);
		result.setTotalTaxValueAdded(totalTaxValueAdded);
		result.setTotalSalePrice(totalSalePrice);

		return result;
	}
}
